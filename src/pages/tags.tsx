import React from 'react';
/* Components */
/* Instruments */
import { icons, TagEnum } from '../theme/icons/tag';

export const Tags: React.FC = () => {
  const navigationElements = [
    { text: TagEnum.REACT, icon: <icons.React /> },
    { text: TagEnum.NODE_JS, icon: React.createElement(icons[TagEnum.NODE_JS]) },
    { text: TagEnum.JAVASCRIPT, icon: <icons.JavaScript /> },
    { text: TagEnum.TYPESCRIPT, icon: <icons.TypeScript /> },
    { text: TagEnum.NEXT_JS, icon: React.createElement(icons[TagEnum.NEXT_JS]) },
    { text: TagEnum.VSCODE, icon: <icons.VSCode /> },
    { text: TagEnum.CSS, icon: <icons.CSS /> },
    { text: TagEnum.GIT, icon: <icons.Git /> },
    { text: TagEnum.GRAPHQL, icon: <icons.GraphQL /> },
    { text: TagEnum.UI_UX, icon: React.createElement(icons[TagEnum.UI_UX]) },
    { text: TagEnum.NPM, icon: <icons.Npm /> },
    { text: TagEnum.TESTING, icon: <icons.Testing /> },
    { text: TagEnum.MACOS, icon: <icons.macOS /> },
    { text: TagEnum.TOOLS, icon: <icons.Tools /> }
  ];
  return (
    <div className="tags">
      {navigationElements.map((el, index) =>
        <span className="tag" data-active="true" key={index}>{el.icon} {el.text}</span>
      )}
    </div>
  )
};