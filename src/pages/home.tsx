import React from 'react';
/* Components */
import { Nav } from '../components/Nav';
import { Tip } from '../components/Tip';

/* Instruments */
import { Tags } from './tags';

export const HomePage: React.FC = () => {
  return (
    <section className="layout">
      <section className="hero">
        <div className="title">
          <h1>Типсы и Триксы</h1>
          <h2>Все темы</h2>
        </div>
        <Tags />
      </section>
      <Nav />
      <section className="tip-list">
        <Tip />
        <Tip />
        <Tip />
      </section>
    </section>
  )
};